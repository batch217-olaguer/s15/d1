console.log("Mabuhay");

alert("Hello Again!");

console. log ( "Hello World!"    ) ;

console. 
log
(
"hello me"
	)

//[SECTION] - Making Comments in JS

// Comments are parts of the code that gets ignored by the language
// Comments are meant to describe the written code

/*		
	1. Single-Line comment - Ctrl + /
	2. Multi-Line comment - Ctrl + Shift + /

*/

// [SECTION] - Variables
// used to contain data
// usually stored in a computer's memory.

// Declaration of variable


// Syntax --> let variableName;
let myVariable;

console.log(myVariable);


// Syntax --> let variableName = variableValue
let mySecondVariable = 2;

console.log(mySecondVariable)

let productName = "desktop computer"
console.log(productName)

// Reassigning value 
let friend;
friend="Kate";
friend="Jane";
friend="Al"
console.log(friend)

// Syntax const variableName variableValue
const pet="Bruno";
secondPet="Lala";
console.log(pet);

// const hoursPerDay=24;

// Local and Global Variables
let outerVariable="hello"; // This is a Global Variable
{
	let innerVariable="Hello World!"; // Local Variable
	console.log(innerVariable);
}
console.log(outerVariable);

const outerExample="Global Variable";
{
	const innerExample="Local Variable";
	console.log(innerExample);
}
console.log(outerExample);

// Multiple Declaration
let productCode="DC017", productBrand="Dell";

/*let productCode="DC017";
let productBrand="Dell";*/

console.log(productCode, productBrand);


// [SECTION] Data Type

// Strings are series of characters that create a word, a phrase, a sentence or anything related to creating text
// Strongs in Java Scrip can be written using either a single (') or double (") qoute.


// Concatenating Strings
// Multiple string values can be combined to create a single string using the "+" symbol
let country="Philippines";
let province="Bicol";

let fullAddress=province+", "+country;
console.log(fullAddress)

let greeting="I live in the"+" "+country;
console.log(greeting)

// The escape character (\) in strings in combination with otehr characters can produce different effects
// "\n" refers to creatin a new line in between text
let mailAddress="Albay\n\nPhilippines";
console.log(mailAddress);

// Using the double quotes along with the single quotes can allow us to easily include single quotes in texts without using the escape character.

let message="John's employees went home early.";
message='John\'s employees went home early';
message="John's employees \"went home early\"";
console.log(message);

// Data type Numbers
let headcount=26;
headcount=2.56653434;
console.log(headcount);

// Decimal numbers or fractions
let grade=98.7;
console.log(grade);

// Exponential Notation
let planetDistance=2e10;
console.log(planetDistance);

// Combining strings and intergers/numbers
console.log("My grade last sem is "+grade);

// Bollean
// Boolean values are normally used to store values relating to the state of certain things.
let isMarried=false;
let inGoodConduct=true;
console.log("is Married: " + isMarried);
console.log("in Good Conduct: " + inGoodConduct);

// Data Type - Arrays
// Arrays are special kind of data type that's used to store multiple values 
// Arrays can store different data types but is normally used to store similar data types
// let/constant arrayName=[elementA, elementB, ElementC, ...]
let english=90;
let math=100;
console.log(english);

let grades=[98.7, 92.1, 90.2, 94.6];
console.log(grades);

// Array with different Data types
// Avoid Doing this. As it is confusing.
let details=["John", "Smith", 32, true];
console.log(details);

// Objects are another special kind of data types that is uised to mimic real world objects/items
// Syntax
		// let/const objectName={
		// 		propertyA; value,
		// 		propertyB; value,
		// 	}

let person={
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contacts: ["09123456789", "09999999999"],
	address: { //nested object - object inside an object
		houseNumber: "345",
		city: "Manila"
	}
}
console.log(person);

// Re-assigning values in an array
const anime=["one piece", "one punch man", "attack on titan"];
anime[4]="Kimetsu no Yaiba";
console.log(anime);